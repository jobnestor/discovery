/* #![deny(unsafe_code)]
#![no_main]
#![no_std]

use rtt_target::rprintln;
use cortex_m_rt::entry;
use rtt_target::rtt_init_print;
use panic_rtt_target as _;
use microbit::{
    board::Board,
    display::blocking::Display,
    hal::{prelude::*, Timer},
};

#[entry]
fn main() -> ! {
    rtt_init_print!();

    let board = Board::take().unwrap();
    let mut timer = Timer::new(board.TIMER0);
    let mut display = Display::new(board.display_pins);
    let light_it_all =[[1; 5]; 5];
	let mut all_dark = [[0; 5]; 5];
	let mut array = [[0; 5 ]; 5];

	let i = 0;
	let j = 0;

    loop {
		for i in 0..4	{
			i += 1;
			j += 1;
			array = light_one(i, j, &mut all_dark);
        	display.show(&mut timer, array, 1000);
        	display.clear();
        	timer.delay_ms(1000_u32);
		}
    }
}

fn light_one(x: i32, y: i32, arr: &mut [[i32; 5]; 5]) -> u32	{
	arr = arr[x][y];
	return 0;
}
 */

 #![deny(unsafe_code)]
#![no_main]
#![no_std]

use cortex_m_rt::entry;
use rtt_target::rtt_init_print;
use panic_rtt_target as _;
use microbit::{
    board::Board,
    display::blocking::Display,
    hal::Timer,
};

const PIXELS: [(usize, usize); 16] = [
    (0,0), (0,1), (0,2), (0,3), (0,4), (1,4), (2,4), (3,4), (4,4),
    (4,3), (4,2), (4,1), (4,0), (3,0), (2,0), (1,0)
];

#[entry]
fn main() -> ! {
    rtt_init_print!();

    let board = Board::take().unwrap();
    let mut timer = Timer::new(board.TIMER0);
    let mut display = Display::new(board.display_pins);
    let mut leds = [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
    ];

    let mut last_led = (0,0);

    loop {
        for current_led in PIXELS.iter() {
            leds[last_led.0][last_led.1] = 0;
            leds[current_led.0][current_led.1] = 1;
            display.show(&mut timer, leds, 300);
            last_led = *current_led;
        }
    }
}
